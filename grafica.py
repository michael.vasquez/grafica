import pygame
import sys

pygame.init()
# colores
blanco = (255, 255, 255)
negro = (0, 0, 0)
limon = (155, 255, 100)
azul = (8, 70, 120)
# medidas
altura = 480
ancho = 640
# puntos
puntoA = (400, 120)
puntoB = (520, 40)
puntoC = (200, 280)
puntoD = (320, 240)

pantalla = pygame.display.set_mode((ancho, altura), 0, 32)
pygame.display.set_caption("Grafica")

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    pantalla.fill(negro)
    pygame.draw.line(pantalla, blanco, [0, (altura / 2)], [ancho, (altura / 2)], 5)  # ------- eje x
    pygame.draw.line(pantalla, blanco, [40, 230], [40, 250], 5)  # -------- -7
    pygame.draw.line(pantalla, blanco, [80, 230], [80, 250], 5)  # --------- -6
    pygame.draw.line(pantalla, blanco, [120, 230], [120, 250], 5)  # -------- -5
    pygame.draw.line(pantalla, blanco, [160, 230], [160, 250], 5)  # -------- -4
    pygame.draw.line(pantalla, blanco, [200, 230], [200, 250], 5)  # -------- -3
    pygame.draw.line(pantalla, blanco, [240, 230], [240, 250], 5)  # ------- -2
    pygame.draw.line(pantalla, blanco, [280, 230], [280, 250], 5)  # -------- -1
    pygame.draw.line(pantalla, blanco, [360, 230], [360, 250], 5)  # -------- 1
    pygame.draw.line(pantalla, blanco, [400, 230], [400, 250], 5)  # --------- 2
    pygame.draw.line(pantalla, blanco, [440, 230], [440, 250], 5)  # --------- 3
    pygame.draw.line(pantalla, blanco, [480, 230], [480, 250], 5)  # --------- 4
    pygame.draw.line(pantalla, blanco, [520, 230], [520, 250], 5)  # ----------- 5
    pygame.draw.line(pantalla, blanco, [560, 230], [560, 250], 5)  # ---------- 6
    pygame.draw.line(pantalla, blanco, [600, 230], [600, 250], 5)  # ---------- 7

    pygame.draw.line(pantalla, blanco, [(ancho / 2), 0], [(ancho / 2), altura], 5)  # --------- eje y
    pygame.draw.line(pantalla, blanco, [310, 40], [330, 40], 5)  # ------------ 5
    pygame.draw.line(pantalla, blanco, [310, 80], [330, 80], 5)  # ----------- 4
    pygame.draw.line(pantalla, blanco, [310, 120], [330, 120], 5)  # ---------- 3
    pygame.draw.line(pantalla, blanco, [310, 160], [330, 160], 5)  # ----------- 2
    pygame.draw.line(pantalla, blanco, [310, 200], [330, 200], 5)  # ----------- 1
    pygame.draw.line(pantalla, blanco, [310, 280], [330, 280], 5)  # ----------- -1
    pygame.draw.line(pantalla, blanco, [310, 320], [330, 320], 5)  # ----------- -2
    pygame.draw.line(pantalla, blanco, [310, 360], [330, 360], 5)  # ------------ -3
    pygame.draw.line(pantalla, blanco, [310, 400], [330, 400], 5)  # ------------- -4
    pygame.draw.line(pantalla, blanco, [310, 440], [330, 440], 5)  # ------------- -5
    # Representan los punto:
    pygame.draw.circle(pantalla, azul, puntoD, 10)
    pygame.draw.circle(pantalla, azul, puntoC, 10)
    pygame.draw.circle(pantalla, azul, puntoB, 10)
    pygame.draw.circle(pantalla, azul, puntoA, 10)
    pygame.draw.polygon(pantalla, limon, (puntoD, puntoB, puntoA, puntoC))
    pygame.display.flip()
